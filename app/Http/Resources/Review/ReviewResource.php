<?php

namespace App\Http\Resources\Review;
use App\model\Product;
use Illuminate\Http\Resources\Json\Resource;

class ReviewResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'customer name' => $this->customer,
            'review' => $this->review,
            'star' => $this->star,
            'product' => Product::find($this->product_id)->name
        ];
    }
}
